
var express = require('express');
var cors=require('cors')// Cross Origin Resource Sharing허용  Access-Control-Allow-Origin: * (전부)
var app = express();
var sqlite3 = require('sqlite3').verbose();
var Query=require('./Query');
const fs = require('fs');


try {
  if (fs.existsSync('./db/my.db')) {
  
    var db = new sqlite3.Database('./db/my.db', sqlite3.OPEN_READWRITE, (err) => {
      if (err) {
        console.error(err.message);
      } else {
   
      }
    });
   
  } else {
   var db = new sqlite3.Database('./db/my.db');
    console.log('Create to the mydb database.');
    db.serialize(()=>{
      db.each(Query.createQuery());
      db.each(Query.dummyDataQuery());
    });
  }
} catch (err) {
  console.error(err)
}




var myLogger = function (req, res, next) {
  console.log('LOGGED');
  next();
};

app.use(myLogger);
app.use(cors());

app.get('/', function (req, res) {
  console.log('path : /');
  res.send('hello world!');
});

app.get('/getList', function (req, res) {
 db.serialize();
 db.all(Query.selectData(),(err,row)=>{
  console.log(row);
  res.send(row);

  });
});

app.get('/insetItem', function (req, res) {
  
  console.log('path : /insetItem');
  var id=req.query.id;
  var text=req.query.text;

  db.serialize();
  db.each(Query.insertItem(id,text))
  res.send('insert');
 });
 
 app.get('/updateItem', function (req, res) {
  
  console.log('path : /updateItem');
  var id=req.query.id;
  var text=req.query.text;
  var done=req.query.done;

  db.serialize();
  db.each(Query.updateItem(id,text,done))
  res.send('update');
 });
 
 app.get('/deleteItem', function (req, res) {
  
  console.log('path : /deleteItem');
  var id=req.query.id;
  
  db.serialize();
  db.each(Query.deleteItem(id))
  res.send('delete');
 });
 

app.listen(3000, function () {
  console.log('exmple app');
});
